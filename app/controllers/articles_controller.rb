class ArticlesController < ApplicationController
  before_action :set_article, only: [:update, :edit, :destroy]
  def new
    @article = Article.new
  end

  def show
    @article = Article.find(params[:id])
    @comments = @article.comments.where.not(id: nil).order('created_at DESC')
  end

  

  def index
    if params[:search].present?
      @articles = Article.search_articles(params[:search])
      if @articles.present?
        respond_to do |format|
          format.js
        end
      else
        # TODO
      end
    else
      @articles = Article.all
    end
    @article = Article.new
    if @articles.present? 
      @articles = @articles.order('created_at desc').paginate(:page => params[:page], :per_page => 6 )
    end
  end

  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    if @article.save
      if params[:article][:image]
        params[:article][:image].each { |image|
          @article.pictures.create(image: image)
        }
      end

    end
    if @article.errors.present?
      flash[:warning] = @article.errors.full_messages.join(", ")
      render :new
    else 
      respond_to do |format|
        format.html { redirect_to articles_path }
        format.js
      end
    end
  end

  def edit; end

  def update   
    respond_to do |format|
      if @article.update(article_params)
        if params[:article][:image]
          params[:article][:image].each { |image|
            @article.pictures.create(image: image)
          }
        end
        if params[:page_index] == "true"
          format.js
        else
          format.html { redirect_to article_path(@article) }
        end
      else
        format.html { render 'edit' }
        format.js
      end
    end
  end
  
  def destroy
    @article.destroy
    respond_to do |format|
      if params[:page_index]
        format.js
      else
        format.html { redirect_to articles_path }
      end
    end
  end
  private
  def article_params
    params.require(:article).permit(:title, :text, :pictures)
  end
  
  def set_article
    @article = current_user.articles.find(params[:id])
  end

end
