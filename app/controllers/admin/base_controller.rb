class Admin::BaseController < ApplicationController
	skip_before_action :require_login
  before_action :authenticate_admin!
end
