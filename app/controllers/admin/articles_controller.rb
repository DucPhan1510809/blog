class Admin::ArticlesController < Admin::BaseController
	def show
		@article = Article.find(params[:id])
	end
	def index
		@articles = Article.order('created_at desc').paginate(:page => params[:page], :per_page => 7 )
	end
	def edit
		@article = Article.find(params[:id])
	end
	def update
	  @article = Article.find(params[:id])
	 
	  if @article.update(article_params)
	    redirect_to admin_articles_path(@article)
	  else
	    render 'edit'
	  end
	end
	def destroy
		@article = Article.find(params[:id])
		@article.destroy
		redirect_to admin_articles_path
	end
	private
	def article_params
		params.require(:article).permit(:title, :text)
	end
end