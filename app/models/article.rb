class Article < ApplicationRecord
	belongs_to :user
	has_many :comments, dependent: :destroy 
	has_many :pictures, dependent: :destroy
  validates :title, presence: true, length: { minimum: 5 }
  accepts_nested_attributes_for :comments, allow_destroy: true, reject_if: :all_blank
	def self.search_articles(search)
		if search.present? 
    	where("LOWER(title) LIKE ? OR LOWER(text) LIKE ?", "%#{search.downcase}%","%#{search.downcase}%")
  	else
  		all
    end
  end
  	
	# validate :validate_title


	# def validate_title
	# 	if !self.title.present?
	# 		errors.add(title: "Wrong!!!!!!")
	# 	end
	# end
end
