class User < ApplicationRecord
	has_secure_password
	has_many :articles, dependent: :destroy 
	has_many :comments, dependent: :destroy
	has_attached_file :avatar, styles: { large: "600x600>" , medium: "300x300>", thumb: "230x230#"}
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
  accepts_nested_attributes_for :articles, allow_destroy: true, reject_if: :all_blank
  validates_presence_of :full_name, :name
  validates_uniqueness_of :name

end
